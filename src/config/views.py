from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer
from rest_framework.request import Request
from rest_framework.response import Response


@api_view(("GET",))
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def health(request: Request, *args, **kwargs) -> Response:
    return Response({"status": "healthy"})
