from django.test import TestCase

from user.models import User


class TestUserManager(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.manager = User.objects

    def test_create_user(self) -> None:
        # given
        data = {"email": "example1@email.com", "password": "st51]gsdfy5"}
        # when
        user = self.manager.create_user(**data)
        # then
        self.assertEqual(user.email, data["email"])
        self.assertTrue(user.check_password(data["password"]))
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

    def test_create_super_user(self) -> None:
        # given
        data = {"email": "example2@email.com", "password": "st51]gsdfy5"}
        # when
        user = self.manager.create_superuser(**data)
        # then
        self.assertEqual(user.email, data["email"])
        self.assertTrue(user.check_password(data["password"]))
        self.assertTrue(user.is_staff)
        self.assertTrue(user.is_superuser)
